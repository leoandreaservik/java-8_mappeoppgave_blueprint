package com.visma.lecture.service;

import com.visma.lecture.common.domain.Item;
import com.visma.lecture.common.domain.support.ItemLocation;
import com.visma.lecture.common.domain.support.ItemType;
import com.visma.lecture.common.exception.InvalidCriteriaException;
import com.visma.lecture.common.exception.NoItemFoundForCriteriaException;
import com.visma.lecture.repository.ShopRepository;
import com.visma.lecture.util.ShopTestUtil;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;
import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.StringStartsWith.startsWith;
import static org.junit.Assert.assertThat;

public class ShopServiceTest {
	
	@Rule
	public ExpectedException expected = ExpectedException.none();
	
	private ShopRepository shopRepository;
	private ShopService shopService;
	
	
	@Before
	public void setUp() throws Exception {
		shopRepository = new ShopRepository(new ShopTestUtil().getItems());
		shopService = new ShopService(shopRepository);
	}
	
	@Test
	public void findItemById() throws Exception {
		shopRepository.create(new Item(1, "Producer Name", ItemLocation.OSLO, ItemType.ELECTRONICS, 2));
		Item itemById = shopService.findItemById(1);
		
		assertThat(itemById.getItemID(), is(1));
		assertThat(itemById.getItemName(), startsWith("Producer"));
		assertThat(itemById.getItemLocation(), is(ItemLocation.OSLO));
		assertThat(itemById.getItemType(), is(ItemType.ELECTRONICS));
		assertThat(itemById.getStock(), is(2));
	}
	
	@Test
	public void shouldThrowExceptionOnMissingInput() throws Exception {
		expected.expect(InvalidCriteriaException.class);
		expected.expectMessage("Input was null, empty or lower than 0.");
		
		shopService.listOfItemsWhereNameStartsWithX(null);
	}
	
	@Test
	public void shouldThrowExceptionOnNoItemsFound() throws Exception {
		expected.expect(NoItemFoundForCriteriaException.class);
		expected.expectMessage("No items were found for the given search criteria.");
		
		shopService.listOfItemsWhereNameStartsWithX("+++");
	}
	
	@Test
	public void mapOfItemsPerLocation() throws Exception {
		Map<ItemLocation, List<Item>> itemLocationListMap = shopService.mapOfItemsPerLocation();
		
		assertThat(itemLocationListMap.keySet().size(), is(2));
		assertThat(itemLocationListMap.get(ItemLocation.OSLO).size(), is(3));
		assertThat(itemLocationListMap.get(ItemLocation.HAMAR).size(), is(3));
		assertThat(itemLocationListMap.get(ItemLocation.OSLO).get(0).getItemID(), is(2001));
	}
	
	@Test
	public void mapOfItemsPerType() throws Exception {
		Map<ItemType, List<Item>> itemTypeListMap = shopService.mapOfItemsPerType();
		
		assertThat(itemTypeListMap.keySet().size(), is(3));
		assertThat(itemTypeListMap.get(ItemType.BEVERAGE).size(), is(2));
		assertThat(itemTypeListMap.get(ItemType.ELECTRONICS).size(), is(2));
		assertThat(itemTypeListMap.get(ItemType.CLOTHING).size(), is(2));
		assertThat(itemTypeListMap.get(ItemType.BEVERAGE).get(0).getItemID(), is(2001));
	}
	
	@Test
	public void mapOfItemsPerProducer() throws Exception {
		Map<String, List<Item>> stringListMap = shopService.mapOfItemsPerProducer();
		
		assertThat(stringListMap.keySet().size(), is(shopRepository.getAll().size()));
		assertThat(stringListMap.get("Producer1").size(), is(1));
		assertThat(stringListMap.get("Producer1").get(0).getItemID(), is(2001));
	}
	
	@Test
	public void mapOfItemListsByStock() throws Exception {
		shopRepository.create(new Item(1, "Producer Name", ItemLocation.OSLO, ItemType.ELECTRONICS, 2000));
		Map<Boolean, List<Item>> partitioned = shopService.mapOfItemListsByStock();
		
		assertThat(partitioned.keySet().size(), is(2));
		assertThat(partitioned.get(Boolean.TRUE).size(), is(1));
		assertThat(partitioned.get(Boolean.FALSE).size(), is(shopRepository.getAll().size() - 1));
	}
	
	@Test
	public void stringOfAllProducersDelimited() throws Exception {
		String s = shopService.stringOfAllProducersDelimited("-");
		
		assertThat(s, is("Producer1-Producer2-Producer3-Producer4-Producer5-Producer6"));
	}
	
	@Test
	public void listOfAllLocationsWithMoreThanXInStock() throws Exception {
		shopRepository.create(new Item(1, "Producer Name", ItemLocation.OSLO, ItemType.ELECTRONICS, 2));
		List<ItemLocation> locations = shopService.listOfAllLocationsWithMoreThanXInStock(1);
		
		assertThat(locations.size(), is(1));
		assertThat(locations.get(0), is(ItemLocation.OSLO));
	}
	
	@Test
	public void listOfAllLocationsWithLessThanXInStock() throws Exception {
		List<ItemLocation> itemLocations = shopService.listOfAllLocationsWithLessThanXInStock(500);
		
		assertThat(itemLocations.size(), is(2));
	}
	
	@Test
	public void listOfItemsForLocationWithMoreThanXInStock() throws Exception {
		shopRepository.create(new Item(1, "Producer Name", ItemLocation.OSLO, ItemType.ELECTRONICS, 2));
		List<Item> items = shopService.listOfItemsForLocationWithMoreThanXInStock(ItemLocation.OSLO, 1);
		
		assertThat(items.size(), is(1));
		assertThat(items.get(0).getItemID(), is(1));
		assertThat(items.get(0).getItemLocation(), is(ItemLocation.OSLO));
	}
	
	@Test
	public void listOfItemsForLocationWithLessThanXInStock() throws Exception {
		List<Item> items = shopService.listOfItemsForLocationWithLessThanXInStock(ItemLocation.OSLO, 500);
		
		assertThat(items.size(), is(3));
		assertThat(items.get(0).getItemLocation(), is(ItemLocation.OSLO));
	}
	
	@Test
	public void listOfItemsWhereNameStartsWithX() throws Exception {
		List<Item> producers = shopService.listOfItemsWhereNameStartsWithX("Producer1");
		
		assertThat(producers.size(), is(1));
		assertThat(producers.get(0).getItemName(), startsWith("Producer1"));
	}
	
	@Test
	public void averageItemStockForLocationX() throws Exception {
		shopRepository.create(new Item(1, "Producer Name", ItemLocation.OSLO, ItemType.ELECTRONICS, 5));
		Double average = shopService.averageItemStockForLocationX(ItemLocation.OSLO);
		
		assertThat(average, is(2.0d));
	}
	
	@Test
	public void itemMostInStock() throws Exception {
		shopRepository.create(new Item(1, "Producer Name", ItemLocation.OSLO, ItemType.ELECTRONICS, 20));
		shopRepository.create(new Item(2, "Producer_Name Name", ItemLocation.OSLO, ItemType.ELECTRONICS, 21));
		Item item = shopService.itemMostInStock();
		
		assertThat(item.getItemID(), is(2));
		assertThat(item.getStock(), is(21));
	}
	
	@Test
	public void leastInStock() throws Exception {
		shopRepository.create(new Item(1, "Producer Name", ItemLocation.OSLO, ItemType.ELECTRONICS, 0));
		Item item = shopService.leastInStock();
		
		assertThat(item.getItemID(), is(1));
		assertThat(item.getStock(), is(0));
	}
	
	@Test
	public void shouldGetFirstWhenMultipleItemsWithSameStock() throws Exception {
		Item item = shopService.leastInStock();
		
		assertThat(item.getItemID(), is(2001));
		assertThat(item.getStock(), is(1));
	}
	
	@Test
	public void itemsForLocationWithXInStock() throws Exception {
		shopRepository.create(new Item(1, "Producer Name", ItemLocation.OSLO, ItemType.ELECTRONICS, 2));
		List<Item> items = shopService.itemsForLocationWithXInStock(ItemLocation.OSLO, 2);
		
		assertThat(items.size(), is(1));
		assertThat(items.get(0).getItemID(), is(1));
		assertThat(items.get(0).getItemLocation(), is(ItemLocation.OSLO));
	}
	
	@Test
	public void allItemsSortedAlphabeticallyByProducer() throws Exception {
		shopRepository.create(new Item(1, "A Name", ItemLocation.OSLO, ItemType.ELECTRONICS, 2));
		shopRepository.create(new Item(2, "B Name", ItemLocation.DRAMMEN, ItemType.BEVERAGE, 5));
		shopRepository.create(new Item(3, "C Name", ItemLocation.HAMAR, ItemType.CLOTHING, 1));
		List<Item> items = shopService.allItemsSortedAlphabeticallyByProducer();
		
		assertThat(items.size(), is(shopRepository.getAll().size()));
		assertThat(items.get(0).getItemName(), is("A Name"));
		assertThat(items.get(1).getItemName(), is("B Name"));
		assertThat(items.get(2).getItemName(), is("C Name"));
	}
	
	@Test
	public void allItemsSortedAlphabeticallyByItemName() throws Exception {
		shopRepository.create(new Item(1, "Name A", ItemLocation.OSLO, ItemType.ELECTRONICS, 2));
		shopRepository.create(new Item(2, "Name B", ItemLocation.DRAMMEN, ItemType.BEVERAGE, 5));
		shopRepository.create(new Item(3, "Name C", ItemLocation.HAMAR, ItemType.CLOTHING, 1));
		List<Item> items = shopService.allItemsSortedAlphabeticallyByItemName();
		
		assertThat(items.size(), is(shopRepository.getAll().size()));
		assertThat(items.get(0).getItemName(), is("Name A"));
		assertThat(items.get(1).getItemName(), is("Name B"));
		assertThat(items.get(2).getItemName(), is("Name C"));
	}
	
	@Test
	public void allItemsSortedByStockFromLowToHigh() throws Exception {
		shopRepository.create(new Item(3, "Producer3 Item3", ItemLocation.HAMAR, ItemType.CLOTHING, 0));
		List<Item> items = shopService.allItemsSortedByStockFromLowToHigh();
		
		assertThat(items.size(), is(shopRepository.getAll().size()));
		assertThat(items.get(0).getItemID(), is(3));
		assertThat(items.get(0).getStock(), is(0));
	}
	
	@Test
	public void allItemsNoDuplicates() throws Exception {
		shopRepository.create(new Item(2001, "Producer1 Test1", ItemLocation.OSLO, ItemType.BEVERAGE, 1));
		shopRepository.create(new Item(2002, "Producer2 Test2", ItemLocation.OSLO, ItemType.ELECTRONICS, 1));
		List<Item> items = shopService.allItemsNoDuplicates();
		
		assertThat(items.size(), is(6));
	}
	
	@Test
	public void getListOfAllItemsByRange() throws Exception {
		List<Item> list = shopService.flatMapByRanges(2001, 2002, 2005, 2006);
		
		assertThat(list.size(), is(4));
		assertThat(list.get(0).getItemID(), is(2001));
		assertThat(list.get(list.size() - 1).getItemID(), is(2006));
	}
	
	@Test
	public void getListOfAllItemsByCriteria() throws Exception {
		List<Item> list = shopService.flatMapByCriteria(ItemLocation.OSLO, ItemType.ELECTRONICS, "Producer1");
		
		assertThat(list.size(), is(4));
		assertThat(list.get(0).getItemID(), is(2001));
		assertThat(list.get(1).getItemID(), is(2002));
		assertThat(list.get(2).getItemID(), is(2003));
		assertThat(list.get(3).getItemID(), is(2005));
	}
	
	@Test
	public void totalStock() throws Exception {
		Double totalStockForAll = shopService.getTotalStockForAll();
		
		assertThat(totalStockForAll, is(6d));
	}
}