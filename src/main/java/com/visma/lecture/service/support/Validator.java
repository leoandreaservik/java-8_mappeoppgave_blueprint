package com.visma.lecture.service.support;

import com.visma.lecture.common.domain.Item;
import com.visma.lecture.common.domain.support.ItemLocation;
import com.visma.lecture.common.exception.InvalidCriteriaException;
import com.visma.lecture.common.exception.NoItemFoundForCriteriaException;

import java.util.List;
import java.util.Map;

/**
 * Validator for shop
 *
 * @author Leo-Andreas Ervik
 */
public class Validator {
	
	public static final String NOT_FOUND_ERROR_MESSAGE = "No items were found for the given search criteria.";
	
	private static final String INPUT_ERROR_MESSAGE = "Input was null, empty or lower than 0.";
	
	
	public static void validateItem(Item item) {
		if (item == null ||
				item.getItemID() == null ||
				item.getItemName() == null ||
				item.getItemName() == null ||
				item.getItemType() == null ||
				item.getItemLocation() == null ||
				item.getStock() == null) {
			throw new InvalidCriteriaException(INPUT_ERROR_MESSAGE);
		}
	}
	
	public static void validateInput(Integer i) {
		if (i == null || i < 0) {
			throw new InvalidCriteriaException(INPUT_ERROR_MESSAGE);
		}
	}
	
	public static void validateInput(String s) {
		if (s == null || s.isEmpty()) {
			throw new InvalidCriteriaException(INPUT_ERROR_MESSAGE);
		}
	}
	
	public static void validateInput(Item i, Integer in) {
		validateItem(i);
		if (in == null || in < 0) {
			throw new InvalidCriteriaException(INPUT_ERROR_MESSAGE);
		}
	}
	
	public static void validateInput(ItemLocation iL) {
		if (iL == null) {
			throw new InvalidCriteriaException(INPUT_ERROR_MESSAGE);
		}
	}
	
	public static void validateInput(Integer i, Integer i2) {
		if (i == null || i2 == null || i < 0 || i2 < 0) {
			throw new InvalidCriteriaException(INPUT_ERROR_MESSAGE);
		}
	}
	
	public static void validateInput(ItemLocation iL, Integer i2) {
		if (iL == null || i2 == null) {
			throw new InvalidCriteriaException(INPUT_ERROR_MESSAGE);
		}
	}
	
	public static void validateOutput(String s) {
		if (s == null || s.isEmpty()) {
			throw new NoItemFoundForCriteriaException(NOT_FOUND_ERROR_MESSAGE);
		}
	}
	
	public static void validateOutput(List<?> items) {
		if (items == null || items.isEmpty()) {
			throw new NoItemFoundForCriteriaException(NOT_FOUND_ERROR_MESSAGE);
		}
	}
	
	public static void validateOutput(Map<?, List<Item>> items) {
		if (items == null || items.isEmpty() || items.entrySet().isEmpty() || items.keySet().isEmpty()) {
			throw new NoItemFoundForCriteriaException(NOT_FOUND_ERROR_MESSAGE);
		}
	}
}
