package com.visma.lecture.service;

import com.visma.lecture.common.domain.Item;
import com.visma.lecture.common.domain.support.ItemLocation;
import com.visma.lecture.common.domain.support.ItemType;
import com.visma.lecture.common.exception.NoItemFoundForCriteriaException;
import com.visma.lecture.repository.ShopRepository;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.visma.lecture.service.support.Validator.*;

/**
 * Service class for shop
 *
 * @author Leo-Andreas Ervik
 */
public class ShopService {
	
	private final ShopRepository shopRepository;
	
	public ShopService(ShopRepository shopRepository) {
		this.shopRepository = shopRepository;
	}
	
	public Item findItemById(Integer id) {
		validateInput(id);
		
		return Optional.of(shopRepository.findItemById(id))
				.orElseThrow(() -> new NoItemFoundForCriteriaException(NOT_FOUND_ERROR_MESSAGE));
	}
	
	public Map<ItemLocation, List<Item>> mapOfItemsPerLocation() {
		Map<ItemLocation, List<Item>> collect = shopRepository.getAll().stream()
				.collect(Collectors.groupingBy(Item::getItemLocation));
		validateOutput(collect);
		
		return collect;
	}
	
	public Map<ItemType, List<Item>> mapOfItemsPerType() {
		Map<ItemType, List<Item>> collect = shopRepository.getAll().stream()
				.collect(Collectors.groupingBy(Item::getItemType));
		validateOutput(collect);
		
		return collect;
	}
	
	public Map<String, List<Item>> mapOfItemsPerProducer() {
		Map<String, List<Item>> collect = shopRepository.getAll().stream()
				.collect(Collectors.groupingBy(i -> i.getItemName().split(" ")[0]));
		validateOutput(collect);
		
		return collect;
	}
	
	public Map<Boolean, List<Item>> mapOfItemListsByStock() {
		Map<Boolean, List<Item>> collect = shopRepository.getAll().stream()
				.collect(Collectors.partitioningBy(i -> i.getStock() > 1500));
		validateOutput(collect);
		
		return collect;
	}
	
	public String stringOfAllProducersDelimited(String delimiter) {
		validateInput(delimiter);
		String collect = shopRepository.getAll().stream()
				.map(item -> item.getItemName().split(" ")[0])
				.collect(Collectors.joining(delimiter));
		validateOutput(collect);
		
		return collect;
	}
	
	public List<ItemLocation> listOfAllLocationsWithMoreThanXInStock(Integer stock) {
		validateInput(stock);
		List<ItemLocation> collect = shopRepository.getAll().stream()
				.filter(i -> i.getStock() > stock)
				.map(Item::getItemLocation)
				.distinct()
				.collect(Collectors.toList());
		validateOutput(collect);
		
		return collect;
	}
	
	public List<ItemLocation> listOfAllLocationsWithLessThanXInStock(Integer stock) {
		validateInput(stock);
		List<ItemLocation> collect = shopRepository.getAll().stream()
				.filter(i -> i.getStock() < stock)
				.map(Item::getItemLocation)
				.distinct()
				.collect(Collectors.toList());
		validateOutput(collect);
		
		return collect;
	}
	
	public List<Item> listOfItemsForLocationWithMoreThanXInStock(ItemLocation location, Integer stock) {
		validateInput(location, stock);
		List<Item> collect = shopRepository.getItemsByLocation(location).stream()
				.filter(i -> i.getStock() > stock)
				.collect(Collectors.toList());
		validateOutput(collect);
		
		return collect;
	}
	
	public List<Item> listOfItemsForLocationWithLessThanXInStock(ItemLocation location, Integer stock) {
		validateInput(location, stock);
		List<Item> collect = shopRepository.getItemsByLocation(location).stream()
				.filter(i -> i.getStock() < stock)
				.collect(Collectors.toList());
		validateOutput(collect);
		
		return collect;
	}
	
	public List<Item> listOfItemsWhereNameStartsWithX(String prefix) {
		validateInput(prefix);
		List<Item> collect = shopRepository.getAll().stream()
				.filter(i -> i.getItemName().startsWith(prefix))
				.collect(Collectors.toList());
		validateOutput(collect);
		
		return collect;
	}
	
	public Double averageItemStockForLocationX(ItemLocation location) {
		validateInput(location);
		return shopRepository.getItemsByLocation(location).stream()
				.mapToInt(Item::getStock)
				.average()
				.orElseThrow(() -> new NoItemFoundForCriteriaException(NOT_FOUND_ERROR_MESSAGE));
	}
	
	public Item itemMostInStock() {
		return shopRepository.getAll().stream()
				.max(Comparator.comparing(Item::getStock))
				.orElseThrow(() -> new NoItemFoundForCriteriaException(NOT_FOUND_ERROR_MESSAGE));
	}
	
	public Item leastInStock() {
		return shopRepository.getAll().stream()
				.min(Comparator.comparing(Item::getStock))
				.orElseThrow(() -> new NoItemFoundForCriteriaException(NOT_FOUND_ERROR_MESSAGE));
	}
	
	public List<Item> itemsForLocationWithXInStock(ItemLocation location, Integer stock) {
		validateInput(location, stock);
		
		List<Item> collect = shopRepository.getItemsByLocation(location).stream()
				.filter(i -> i.getStock().equals(stock))
				.collect(Collectors.toList());
		validateOutput(collect);
		
		return collect;
	}
	
	public List<Item> allItemsSortedAlphabeticallyByProducer() {
		List<Item> collect = shopRepository.getAll().stream()
				.sorted((i1, i2) -> i1.getItemName().split(" ")[0]
						.compareToIgnoreCase(i2.getItemName().split(" ")[0]))
				.collect(Collectors.toList());
		validateOutput(collect);
		
		return collect;
	}
	
	public List<Item> allItemsSortedAlphabeticallyByItemName() {
		List<Item> collect = shopRepository.getAll().stream()
				.sorted((i1, i2) -> i1.getItemName().split(" ")[1]
						.compareToIgnoreCase(i2.getItemName().split(" ")[1]))
				.collect(Collectors.toList());
		validateOutput(collect);
		
		return collect;
	}
	
	public List<Item> allItemsSortedByStockFromLowToHigh() {
		List<Item> collect = shopRepository.getAll().stream()
				.sorted(Comparator.comparing(Item::getStock))
				.collect(Collectors.toList());
		validateOutput(collect);
		
		return collect;
	}
	
	public List<Item> allItemsNoDuplicates() {
		List<Item> collect = shopRepository.getAll().stream()
				.distinct()
				.collect(Collectors.toList());
		validateOutput(collect);
		
		return collect;
	}
	
	public List<Item> flatMapByRanges(Integer range1, Integer range2, Integer range3, Integer range4) {
		Stream<List<Item>> itemsByRange = Stream.of(shopRepository.getItemsByRange(range1, range2), shopRepository.getItemsByRange(range3, range4));
		
		return itemsByRange
				.flatMap(Collection::stream)
				.collect(Collectors.toList());
	}
	
	public List<Item> flatMapByCriteria(ItemLocation location, ItemType type, String producer) {
		Stream<List<Item>> items = Stream.of(shopRepository.getItemsByLocation(location),
				shopRepository.getItemsByType(type),
				shopRepository.getItemByProducer(producer));
		
		return items
				.flatMap(Collection::stream)
				.distinct()
				.collect(Collectors.toList());
	}
	
	public Double getTotalStockForAll() {
		return shopRepository.getAll().stream()
				.mapToDouble(Item::getStock)
				.reduce(Double::sum)
				.orElse(0.0d);
	}
	
}
