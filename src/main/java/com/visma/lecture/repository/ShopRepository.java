package com.visma.lecture.repository;

import com.visma.lecture.common.domain.Item;
import com.visma.lecture.common.domain.support.ItemLocation;
import com.visma.lecture.common.domain.support.ItemType;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Repository class for shop
 *
 * @author Leo-Andreas Ervik
 */
public class ShopRepository {
	
	private final List<Item> items;
	
	public ShopRepository(List<Item> items) {
		this.items = items;
	}
	
	public Item findItemById(Integer id) {
		return items.stream()
				.filter(e -> e.getItemID().equals(id))
				.findFirst()
				.orElse(null);
	}
	
	public Boolean create(Item item) {
		return items.add(item);
	}
	
	public Boolean update(Item item) {
		Item i = findItemById(item.getItemID());
		delete(i.getItemID());
		return create(item);
	}
	
	public Boolean delete(Integer itemId) {
		return items.removeIf(e -> e.getItemID().equals(itemId));
	}
	
	public List<Item> getAll() {
		return items;
	}
	
	public List<Item> getItemsByRange(Integer start, Integer end) {
		return items.stream()
				.filter(e -> e.getItemID() >= start && e.getItemID() <= end)
				.collect(Collectors.toList());
	}
	
	public List<Item> getItemsByLocation(ItemLocation itemLocation) {
		return items.stream()
				.filter(e -> e.getItemLocation().equals(itemLocation))
				.collect(Collectors.toList());
	}
	
	public List<Item> getItemsByType(ItemType itemType) {
		return items.stream()
				.filter(e -> e.getItemType().equals(itemType))
				.collect(Collectors.toList());
	}
	
	public List<Item> getItemByProducer(String itemProducer) {
		return items.stream()
				.filter(e -> e.getItemName()
						.split(" ")[0]
						.replace("_", " ")
						.equalsIgnoreCase(itemProducer))
				.collect(Collectors.toList());
	}
	
}
