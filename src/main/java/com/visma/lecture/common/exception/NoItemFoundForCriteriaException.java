package com.visma.lecture.common.exception;

/**
 * Dedicated exception
 *
 * @author ervikleo on 23.10.2017
 */
public class NoItemFoundForCriteriaException extends RuntimeException {
	
	public NoItemFoundForCriteriaException(String message) {
		super(message);
	}
	
	public NoItemFoundForCriteriaException(String message, Throwable cause) {
		super(message, cause);
	}
}
